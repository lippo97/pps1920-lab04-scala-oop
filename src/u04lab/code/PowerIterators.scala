package u04lab.code

import Optionals._
import Lists._
import Streams.Stream

import scala.util.Random

trait PowerIterator[A] {
  def next(): Option[A]
  def allSoFar(): List[A]
  def reversed(): PowerIterator[A]
}

object PowerIterator {
  def apply[A](stream: Stream[A]): PowerIterator[A] = PowerIteratorImpl(stream)

  case class PowerIteratorImpl[A](var stream: Stream[A]) extends PowerIterator[A] {
    var soFar: List[A] = List.Nil()

    override def next(): Option[A] = stream match {
      case Stream.Cons(h, t) =>
        soFar = List.Cons(h(), soFar)
        stream = t()
        Option.Some(h())
      case _ => Option.None()
    }

    override def allSoFar(): List[A] = List.reverse(soFar)

    override def reversed(): PowerIterator[A] = PowerIterator(List.toStream(soFar))
  }
}

trait PowerIteratorsFactory {

  def incremental(start: Int, successive: Int => Int): PowerIterator[Int]
  def fromList[A](list: List[A]): PowerIterator[A]
  def randomBooleans(size: Int): PowerIterator[Boolean]
}

class PowerIteratorsFactoryImpl extends PowerIteratorsFactory {

  override def incremental(start: Int, successive: Int => Int): PowerIterator[Int] =
    PowerIterator(Stream.iterate(start)(successive))

  override def fromList[A](list: List[A]): PowerIterator[A] =
    PowerIterator(List.toStream(list))

  override def randomBooleans(size: Int): PowerIterator[Boolean] =
    PowerIterator(Stream.generate(Random.nextBoolean()))
}
