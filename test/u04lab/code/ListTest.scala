package u04lab.code

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions._
import Lists.List
import Lists.List.{Cons, Nil}
import Streams.Stream
import Optionals.Option
import Optionals.Option.{Some, None}


class ListTest {

  @Test def testListVarArgFactory(): Unit = {
    assertEquals(Cons(1, Cons(2, Cons(3, Cons(4, Cons(5, Nil()))))), List(1, 2, 3, 4, 5))
  }

  @Test def testShouldFindElement(): Unit = {
    assertEquals(List.findFirst(List(1, 2, 3, 4, 5, 6))(_ % 3 == 2), Some(2))
  }

  @Test def testShouldNotFindElement(): Unit = {
    assertEquals(List.findFirst(List(1, 2, 3, 4, 5, 6))(_ == 7), None())
  }

  @Test def testShouldConvertListToStream(): Unit = {
    assertEquals(Stream.toList(List.toStream(List(9, 8, 7, 6, 5))),
      Stream.toList(Stream.take(Stream.iterate(9)(_-1))(5)))
  }

  @Test def testAllElementsShouldMatch(): Unit = {
    assertTrue(List.all(List(1, 1, 1, 1))(_ == 1))
  }

  @Test def testAllElementsShouldNotMatch(): Unit = {
    assertFalse(List.all(List(1, 1, 1, 1))(_ == 2))
  }

  @Test def testEmptyListShouldNotMatch(): Unit = {
    assertFalse(List.all[Int](Nil())(_ == 5))
  }

  @Test def testAllElementsShouldBeEqual(): Unit = {
    assertEquals(List.allEquals(List(1, 1, 1, 1)), Some(1))
  }

  @Test def testNotAllElementsShouldBeEqual(): Unit = {
    assertEquals(List.allEquals(List(1, 1, 1, 2)), None())
  }

  @Test def testEmptyListShouldReturnNone(): Unit = {
    assertEquals(List.allEquals(List()), None())
  }

  @Test def testShouldConvertScalaList(): Unit = {
    assertEquals(List.fromScalaList(collection.immutable.List(1, 2, 3, 4)), List(1, 2, 3, 4))
  }
}
