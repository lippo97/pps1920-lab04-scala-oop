package u04lab.code

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions._
import u04lab.code.Lists.List
import u04lab.code.Lists.List.{Cons, Nil}
class StudentTest {
  val cPPS = Course("PPS","Viroli")
  val cPCD = Course("PCD","Ricci")
  val cSDR = Course("SDR","D'Angelo")
  val s1 = Student("mario",2015)
  val s2 = Student("gino",2016)
  val s3 = Student("rino") //defaults to 2017
  s1.enrolling(cPPS)
  s1.enrolling(cPCD)
  s2.enrolling(cPPS)
  s3.enrolling(cPPS)
  s3.enrolling(cPCD)
  s3.enrolling(cSDR)

  @Test def testCourses(): Unit = {
    assertEquals(Cons("PCD",Cons("PPS",Nil())), s1.courses)
    assertEquals(Cons("PPS",Nil()), s2.courses)
    assertEquals(Cons("SDR",Cons("PCD",Cons("PPS",Nil()))), s3.courses)
  }

  @Test def testHasTeacher(): Unit = {
    assertTrue(s1.hasTeacher("Ricci"))
    assertFalse(s1.hasTeacher("D'Angelo"))
  }

  @Test def testShouldHaveTheSameTeacher(): Unit = {
    val cOOP = Course("OOP","Viroli")
    assertEquals(sameTeacher.unapply(List(cPPS, cOOP)), Some("Viroli"))
  }

  @Test def testShouldNotHaveTheSameTeacher(): Unit = {
    assertEquals(sameTeacher.unapply(List(cPPS, cPCD)), Option.empty)
  }
}
