package u04lab.code

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions._

class ComplexTest {

  @Test def testAdd(): Unit = {
    val n = Complex(2, -1)
    val m = Complex(5, -2)
    assertEquals(Complex(7, -3), n + m)
  }

  @Test def testMul(): Unit = {
    assertEquals(Complex(15, 5), Complex(2, -1) * Complex(5, 5))
    assertEquals(Complex(8, -9), Complex(2, -1) * Complex(5, -2))
  }
}
